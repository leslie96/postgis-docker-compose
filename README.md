# Postgis-docker-compose

#### 介绍
在主流的X86平台搭建Postgis


#### 安装教程


1. 将此postgis文件夹拷贝到服务器内“PG”文件夹（执行前请查看使用说明）
2. 执行"install_postgis.sh"脚本


#### 使用说明

执行"install_postgis.sh"脚本前需要离线安装"postgis/postgis:13-3.0"docker镜像执行以下步骤：

1.去百度网盘路径：xxx下载 “dockerInstall_postgis-13-3.0.tar.gz”安装包。

2.服务器上创建“PG”文件夹，并将“dockerInstall_postgis-13-3.0.tar.gz”安装包放在“PG”文件夹下并解压。
    
-     $ mkdir PG
-     $ cd PG
-     $ tar dockerInstall_postgis-13-3.0.tar.gz


3.进入解压后的文件夹 “dockerInstall_postgis-13-3.0”   赋权限，并执行安装并镜像命令

-     $ cd dockerInstall_postgis-13-3.0
-     $ chmod +x installdockerimages.sh  
-     $ ./installdockerimages.sh 	



                                                                  created by gyf  20201204